# Contributor: Matthias Ahouansou <matthias@ahouansou.cz>
# Maintainer: Matthias Ahouansou <matthias@ahouansou.cz>
pkgname=tree-sitter-xml
pkgver=0.5.0
pkgrel=0
pkgdesc="XML & DTD grammars for tree-sitter"
url="https://github.com/ObserverOfTime/tree-sitter-xml"
arch="all"
license="MIT"
makedepends="tree-sitter-dev"
subpackages="$pkgname-doc"
provides="tree-sitter-dtd=$pkgver-r$pkgrel"
install_if="tree-sitter-grammars"
source="$pkgname-$pkgver.tar.gz::https://github.com/ObserverOfTime/tree-sitter-xml/archive/refs/tags/v$pkgver.tar.gz"
options="!check"  # no tests for shared lib

_langs='xml dtd'

build() {
	local lang; for lang in $_langs; do
		abuild-tree-sitter build -s "$lang/src"
	done
}

package() {
	local lang; for lang in $_langs; do
		DESTDIR="$pkgdir" abuild-tree-sitter install -s "$lang/src" -q "$lang/queries"
	done
	install -Dm644 LICENSE "$pkgdir"/usr/share/licenses/$pkgname/LICENSE
}

sha512sums="
c3cbb6175852d5fba0a82a513b60582ae1a65e588d27c6ac771a2118d742fe00a2bba72c3dcfdd80289390003b45b962a57b7544c2dcd7d44178c2cd44fec052  tree-sitter-xml-0.5.0.tar.gz
"
