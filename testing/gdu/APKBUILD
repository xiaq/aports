# Contributor: Kevin Daudt <kdaudt@alpinelinux.org>
# Maintainer: Kevin Daudt <kdaudt@alpinelinux.org>
pkgname=gdu
pkgver=5.26.0
_majorver=${pkgver%%.*}
pkgrel=0
pkgdesc="Fast disk usage calculator with console interface"
url="https://github.com/dundee/gdu"
arch="all"
license="MIT"
makedepends="go gzip"
subpackages="$pkgname-doc"
source="$pkgname-$pkgver.tar.gz::https://github.com/dundee/gdu/archive/refs/tags/v$pkgver.tar.gz"

export GOFLAGS="$GOFLAGS -modcacherw"
export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

prepare() {
	default_prepare

	rm -rf vendor/
}

build() {
	local versionflags
	versionflags="
	-X github.com/dundee/gdu/v$_majorver/build.Version=$pkgver
	-X github.com/dundee/gdu/v$_majorver/build.User=buildozer
	-X 'github.com/dundee/gdu/v$_majorver/build.Time=$(date)'
	"
	go build -v \
		-ldflags="$versionflags" \
		./cmd/gdu

	gzip gdu.1
}

check() {
	# These tests are failing on 32-bit with: "error:
	# cannot allocate memory while mmapping
	# /tmp/badger/000002.vlog with size: 2147483646"
	case "$CARCH" in
	arm*|x86)
		go test -skip 'TestStoredAnalyzer|TestRemoveStoredFile' -v ./... ;;
	*)
		go test -v ./... ;;
	esac
}

package() {
	install -Dm0755 gdu -t "$pkgdir"/usr/bin/
	install -Dm0644 gdu.1.gz -t "$pkgdir"/usr/share/man/man1/
	install -Dm0644 LICENSE.md -t "$pkgdir"/usr/share/licenses/gdu/
}

sha512sums="
899a1c47350423bbed64335042e8ececa3b8828a22e6b7470c3e4dc13f86b7ada7f485f566df7ab819f669f2e60707b8e44b938e8d42f640e1b35795ff493f06  gdu-5.26.0.tar.gz
"
